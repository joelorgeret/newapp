import firebaseConfig from '../config'
import * as firebase from 'firebase';
import ServicePokemon from './pokemon';

if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig)
}

export const services = {
    pokemon: new ServicePokemon(firebase.app())
}

// export const pokemon = new Pokemon(firebase.app('NewApp'))

export default services