import { useAppContext } from './../context/AppContext';
import * as firebase from 'firebase';
import { Pokemon, Pokemons } from '../types';




class ServicePokemon {
    private _app: firebase.app.App


    constructor(app: firebase.app.App) {
        this._app = app;
    }

    setPokemonLike(id: string, liked: boolean) {
        this._app.database().ref('Pokemons')
            .child(id)
            .child('liked')
            .set(liked);

    }

    async getPokemonLike(id: string): Promise<boolean> {

        const snapshot = await this._app.database().ref('Pokemons')
            .child(id)
            .child('liked')
            .once('value')

        return (snapshot.val())
    }


    async getPokemons(): Promise<Pokemons> {
        const snapshot = await this._app.database().ref('Pokemons')
            .once('value')
        let data: Pokemon[] = []
        snapshot.forEach(item => {
            data.push(item.val())
        })
        return (data)
    }

}

export default ServicePokemon 