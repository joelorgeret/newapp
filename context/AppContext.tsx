import { createContext, useContext, useEffect, useState } from "react"
import React from 'react'
import { Pokemon, Pokemons } from "../types"




type AppContextType = {
    data: Pokemons
    setData: React.Dispatch<React.SetStateAction<Pokemons>>
    likedFilter: boolean
    setLikedFilter: React.Dispatch<React.SetStateAction<boolean>>
    nameFilter: string
    setNameFilter: React.Dispatch<React.SetStateAction<string>>
    typeFilter: string
    setTypeFilter: React.Dispatch<React.SetStateAction<string>>
    idFilter: string
    setIdFilter: React.Dispatch<React.SetStateAction<string>>
    isLoading: boolean
    setIsLoading: React.Dispatch<React.SetStateAction<boolean>>
    setDataLiked: (id: string, liked: boolean) => void
    // filterData: () => Pokemon[]
    // filteredData: Pokemon[]
    // setFilteredData: React.Dispatch<React.SetStateAction<Pokemons>>
}

export const AppContext = createContext<AppContextType>({
    data: [],
    setData: () => { },
    likedFilter: false,
    setLikedFilter: () => { },
    nameFilter: '',
    setNameFilter: () => { },
    typeFilter: '',
    setTypeFilter: () => { },
    idFilter: '',
    setIdFilter: () => { },
    isLoading: true,
    setIsLoading: () => { },
    setDataLiked: () => { },
    // filterData: () => [],
    // filteredData: [],
    // setFilteredData: () => { }
})

const AppContextProvider: React.FC = ({ children }) => {
    const [likedFilter, setLikedFilter] = useState(false)
    const [nameFilter, setNameFilter] = useState('')
    const [typeFilter, setTypeFilter] = useState('')
    const [idFilter, setIdFilter] = useState('')
    const [data, setData] = useState<Pokemons>([])
    const [isLoading, setIsLoading] = useState(true)
    // const [filteredData, setFilteredData] = useState<Pokemons>([])


    const setDataLiked = (id: string, liked: boolean) => {
        data.map(item => {
            if (item.id === id) {
                item.liked = liked
            }
        })
        setData([...data])
    }

    // let filteredData = data.filter(item => (item.liked && likedFilter) || !likedFilter ? true : item.liked)
    // filteredData = filteredData.filter(item => (
    //     item.name.toLowerCase().startsWith(nameFilter.toLowerCase()))
    //     && (item.id.startsWith(idFilter))
    //     && (item.type.filter(item2 => (
    //         item2.toLowerCase().startsWith(typeFilter.toLowerCase())
    //     )).length > 0))

    // const filterData = () => {
    //     let filteredData = data.filter(item => (item.liked && likedFilter) || !likedFilter ? true : item.liked)
    //     filteredData = filteredData.filter(item => (
    //         item.name.toLowerCase().startsWith(nameFilter.toLowerCase()))
    //         && (item.id.startsWith(idFilter))
    //         && (item.type.filter(item2 => (
    //             item2.toLowerCase().startsWith(typeFilter.toLowerCase())
    //         )).length > 0))
    //     return (filteredData)
    // }

    return (<AppContext.Provider value={{
        likedFilter,
        setLikedFilter,
        nameFilter,
        setNameFilter,
        typeFilter,
        setTypeFilter,
        idFilter,
        setIdFilter,
        data,
        setData,
        isLoading,
        setIsLoading,
        setDataLiked,
        // filteredData,
        // setFilteredData,
        // filterData
    }}>{children}</AppContext.Provider>)
}

export const useAppContext = () => {
    return useContext(AppContext)
}

export default AppContextProvider