import { TextInput, StyleSheet, TouchableOpacity, View, Text } from "react-native"
import React from 'react'
import { useAppContext } from "../context/AppContext"
// import CardView from 'react-native-cardview'


const Header: React.FC = () => {
    const styles = func()

    const { likedFilter, setLikedFilter, setNameFilter, setTypeFilter, setIdFilter } = useAppContext()

    const handleClickLiked = () => {
        setLikedFilter(true)
    }

    const handleClickNotLiked = () => {
        setLikedFilter(false)
    }

    return (
        <View style={styles.container}>
            <View style={styles.titleView}>
                <Text style={styles.title}>POKEDEX</Text>
            </View>
            <View style={styles.input}>
                <TextInput style={styles.inputTextInput} placeholder='Search by name' onChangeText={text => (setNameFilter(text))}></TextInput>
                <TextInput style={styles.inputTextInput} placeholder='Search by type' onChangeText={text => (setTypeFilter(text))}></TextInput>
                <TextInput style={styles.inputTextInput} keyboardType='number-pad' placeholder='Search by id' onChangeText={text => (setIdFilter(text))}></TextInput>
            </View>
            <View style={styles.buttonContainer}>
                <TouchableOpacity style={likedFilter ? styles.button : styles.buttonSelected} onPress={handleClickNotLiked}>
                    <Text style={styles.textButton}>POKEDEX</Text>
                </TouchableOpacity>
                <TouchableOpacity style={!likedFilter ? styles.button : styles.buttonSelected} onPress={handleClickLiked}>
                    <Text style={styles.textButton}>LIKED</Text>
                </TouchableOpacity>
            </View>

        </View>
    )
}

const func = () => {
    return (
        StyleSheet.create({
            container: {
                flex: 1,
                flexDirection: "column",
                elevation: 0,
                backgroundColor: 'white',
            },

            buttonContainer: {
                flex: 1,
                flexDirection: 'row',
                alignItems: 'flex-end',
                overflow: 'hidden'
            },

            button: {
                padding: 10,
                margin: 20,
                marginBottom: 0,
                backgroundColor: 'white',
                width: 150,
                elevation: 10,
                shadowOffset: { height: -10, width: 0 },
                borderTopRightRadius: 10,
                borderTopLeftRadius: 10,
            },

            buttonSelected: {
                padding: 10,
                margin: 20,
                marginBottom: 0,
                backgroundColor: '#E5E5E5',
                width: 150,
                borderColor: '#9E9E9E',
                borderWidth: 1,
                borderBottomColor: '#E5E5E5',
                borderTopRightRadius: 10,
                borderTopLeftRadius: 10,
                elevation: 15,
            },

            textButton: {
                alignSelf: 'center',
            },

            titleView: {
                flex: 0,
                alignItems: 'center',
                padding: 25,
            },

            title: {
                fontSize: 25,
            },

            input: {
                // elevation: 1,
                flexDirection: 'row',
                alignContent: 'space-around',
                justifyContent: 'space-around',
            },

            inputTextInput: {
                borderRadius: 5,
                paddingLeft: 5,
                borderWidth: 1,
                borderColor: '#E5E5E5',
                margin: 5,
            },
        })
    )
}

export default Header