import React, { useEffect } from "react"
import { StyleSheet, View, FlatList, ActivityIndicator, Alert } from "react-native"
import { Pokemon } from '../types'
import PokemonCard from "./PokemonCard";
import { useAppContext } from "../context/AppContext";
import { services } from '../services'



const PokemonList = () => {

    const { setData, isLoading, setIsLoading, data, likedFilter, nameFilter, idFilter, typeFilter } = useAppContext()


    useEffect(() => {
        try {
            services.pokemon.getPokemons().then((value) => {
                setData(value)
                setIsLoading(false)
            })
        } catch (e) {
            Alert.alert('Connection  failed with the server')
            throw e
        }
    }, [])

    // frhu

    let filteredData = data.filter(item => (item.liked && likedFilter) || !likedFilter ? true : item.liked)
    filteredData = filteredData.filter(item => (
        item.name.toLowerCase().startsWith(nameFilter.toLowerCase()))
        && (item.id.startsWith(idFilter))
        && (item.type.filter(item2 => (
            item2.toLowerCase().startsWith(typeFilter.toLowerCase())
        )).length > 0))


    const renderItem = ({ item }: { item: Pokemon }) => (
        <View>
            <PokemonCard {...item} />
        </View>
    );

    return (
        <>
            <View style={{ alignSelf: 'center', justifyContent: 'center', flex: 1 }}>
                <ActivityIndicator color='red' animating={isLoading} size='large' />
            </View>
            <FlatList
                data={filteredData}
                renderItem={renderItem}
                keyExtractor={item => item.id}
                style={style.liste}
                refreshing={isLoading}
            >
            </FlatList>
        </>

    )
}

const style = StyleSheet.create({
    liste: {
        elevation: 5
    },

    card: {
        height: 45
    },
})

export default PokemonList