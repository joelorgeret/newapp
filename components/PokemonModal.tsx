import React, { Children, useEffect, useState } from "react"
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native"

import { services } from '../services'

const PokemonModal: React.FC = ({ id, children }) => {

    const [openModal, setOpenModal] = useState(false)
    const [stats, setStats] = useState({})
    // const [isLoading, setIsLoading] = useState(true)

    // const handleOpenModal = () => {
    //     setOpenModal(!openModal)
    // }

    const style = themedStyle()

    return (

        <View style={style.modal}>
            <View>
                <Image style={style.imgModal} source={{ uri: stats.img }} />
            </View>
            <View style={style.headerModal}>
                <Text style={style.idModal}>{id}</Text>
                {/* <Text style={style.titleModal}>{stats.name.toUpperCase()}</Text> */}
            </View>
            <View style={style.containerModal}>
                { }
            </View>
            {children}
        </View>
    )

}


const themedStyle = () => StyleSheet.create({

    modal: {
        backgroundColor: 'white',
        padding: 10,
        elevation: 5,
        alignSelf: 'center',
        marginTop: 'auto',
        marginBottom: 'auto',
        borderRadius: 10,
    },


    headerModal: {
        padding: 10,
        alignContent: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
    },

    idModal: {
        color: '#9E9E9E',
        fontWeight: 'bold',
    },

    titleModal: {
        fontWeight: 'bold',
        marginLeft: 10,
    },

    imgModal: {
        height: 150,
        width: 150,
        marginRight: 20,
        resizeMode: 'contain',
    },


    containerModal: {

    },
})


export default PokemonModal