import { IconButton } from 'react-native-paper';
import React, { useState } from "react"
import { Image, Modal, StyleSheet, Text, TouchableOpacity, View } from "react-native"
import { Pokemon, TypePokemon } from "../types"
import { services } from '../services'
import {
    LineChart,
    BarChart,
    PieChart,
    ProgressChart,
    ContributionGraph,
    StackedBarChart
} from "react-native-chart-kit";
import { useAppContext } from '../context/AppContext';



const ColorTypeList: { [key in TypePokemon]: string } = {
    Grass: '#b7db73',
    Poison: '#9fff88',
    Fire: '#FFB116',
    Flying: '#19F5FF',
    Water: '#62b5f2',
    Bug: '#77b9d9',
    Normal: '#fa7c7c',
    Electric: '#77e6f9',
    Ground: '#c5ad9e',
    Fighting: "#ff79bc",
    Psychic: "#bf93d2",
    Rock: '#aaa79f',
    Ice: '#8edcf3',
    Steel: '#b6b6b6d8',
    Ghost: '#56f1d2',
    Dragon: '#ebda40'
}

const chartConfig = {
    backgroundColor: "#FFFFFF",
    backgroundGradientFrom: "#FFFFFF",
    backgroundGradientTo: "#FFFFFF",
    decimalPlaces: 0, // optional, defaults to 2dp
    color: (opacity = 1) => `rgba(87, 137, 149, ${opacity})`,
    labelColor: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
    style: {
        borderRadius: 16,
    },
    propsForDots: {
        r: "6",
        strokeWidth: "2",
        stroke: "#ffa726"
    }
}




const PokemonCard: React.FC<Pokemon> = ({ name, id, img, type, stats, liked: like }) => {


    const { setDataLiked } = useAppContext()
    const style = themedStyle(name)
    const [liked, setLiked] = useState<boolean>(like ?? false)
    const [openModal, setOpenModal] = useState(false)

    const handleLikeClick = () => {
        services.pokemon.setPokemonLike(id, !liked)
        setDataLiked(id, !liked)
        setLiked(!liked)
    }

    const handleOpenModal = () => {
        setOpenModal(!openModal)
    }

    const statsData = {
        labels: ['ATK', 'DEF', 'HP', 'SPATK', 'SPDEF', 'SPD'],
        datasets: [
            {
                data: [
                    stats?.attack,
                    stats?.defense,
                    stats?.hp,
                    stats?.spattack,
                    stats?.spdefense,
                    stats?.speed,
                ]
            }
        ]
    }
    return (
        <>
            <View style={style.card}>
                <TouchableOpacity onPress={handleOpenModal}>
                    <Image style={style.img} source={{ uri: img }} />
                </TouchableOpacity>
                <View>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={style.id}>{id}</Text>
                        <Text style={style.name}>{name.toUpperCase()}</Text>
                    </View>

                    <View style={style.stats}>
                        {type.map((item: TypePokemon) => {
                            return (
                                <View
                                    style={{
                                        backgroundColor: ColorTypeList[item],
                                        borderRadius: 10,
                                        padding: 2,
                                        margin: 5
                                    }}
                                    key={id + item}
                                >
                                    <Text style={{ color: 'white', paddingRight: 5, paddingLeft: 5 }}>{item}</Text>
                                </View>
                            );
                        })}
                    </View>
                </View>
                <IconButton
                    icon={liked ? "heart" : "heart-outline"}
                    color='red'
                    onPress={handleLikeClick}
                    size={40}
                    style={style.like}
                    accessibilityComponentType={IconButton}
                    accessibilityTraits={IconButton} />
            </View>
            <Modal
                animationType="slide"
                transparent={true}
                visible={openModal}
            >
                {/* <PokemonModal id={id}> */}
                <View style={style.modal}>
                    <View>
                        <Image style={style.imgModal} source={{ uri: img }} />
                    </View>
                    <View style={style.headerModal}>
                        <Text style={style.idModal}>{id}</Text>
                        <Text style={style.titleModal}>{name.toUpperCase()}</Text>
                    </View>
                    <View style={style.stats}>
                        {type.map((item: TypePokemon) => {
                            return (
                                <View
                                    style={{
                                        backgroundColor: ColorTypeList[item],
                                        borderRadius: 10,
                                        padding: 2,
                                        margin: 5
                                    }}
                                    key={id + item}
                                >
                                    <Text style={{ color: 'white', paddingRight: 5, paddingLeft: 5 }}>{item}</Text>
                                </View>
                            );
                        })}
                    </View>
                    <View style={style.containerModal}>
                        <Text style={style.stat}><Image source={require('../img/sword.png')} style={style.icone} /> {stats?.attack}</Text>
                        <Text style={style.stat}><Image source={require('../img/shied.png')} style={style.icone} /> {stats?.defense}</Text>
                        <Text style={style.stat}><Image source={require('../img/coeur_plein.png')} style={style.icone} /> {stats?.hp}</Text>
                        <Text style={style.stat}><Image source={require('../img/spattack.png')} style={style.icone} /> {stats?.spattack}</Text>
                        <Text style={style.stat}><Image source={require('../img/spdefense.png')} style={style.icone} /> {stats?.spdefense}</Text>
                        <Text style={style.stat}><Image source={require('../img/speed.png')} style={style.icone} /> {stats?.speed}</Text>
                    </View>
                    <View>
                        <BarChart
                            data={statsData}
                            width={350}
                            height={220}
                            yAxisLabel=""
                            yAxisSuffix=""
                            yAxisInterval={1} // optional, defaults to 1
                            chartConfig={chartConfig}
                        />
                    </View>
                    <TouchableOpacity onPress={handleOpenModal} style={style.buttonModal}>
                        <Text>Close</Text>
                    </TouchableOpacity>
                </View>
                {/* </PokemonModal> */}


            </Modal>
        </>
    )

}

const themedStyle = (n: string) => StyleSheet.create({
    modal: {
        backgroundColor: 'white',
        padding: 10,
        elevation: 5,
        alignSelf: 'center',
        marginTop: 'auto',
        marginBottom: 'auto',
        borderRadius: 10,
        alignItems: 'center',
    },


    headerModal: {
        padding: 10,
        alignContent: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
    },

    idModal: {
        color: '#9E9E9E',
        fontWeight: 'bold',
    },

    titleModal: {
        fontWeight: 'bold',
        marginLeft: 10,
    },

    imgModal: {
        height: 150,
        width: 150,
        marginRight: 20,
        resizeMode: 'contain',
        alignSelf: 'center',
    },


    containerModal: {
        flexDirection: 'row',
        margin: 10,
    },

    buttonModal: {
        borderRadius: 10,
        backgroundColor: '#E5E5E5',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 5,
        alignSelf: 'stretch',
    },

    card: {
        flexDirection: 'row',
        padding: 10,
        margin: 25,
        borderRadius: 10,
        alignSelf: "stretch",
        justifyContent: 'space-between',
        backgroundColor: 'white',
        elevation: 7,
    },

    img: {
        height: 80,
        width: 80,
        marginRight: 20,
        resizeMode: 'contain',
        // backgroundColor: n
    },

    stats: {
        alignItems: "center",
        flexDirection: 'row',
    },

    like: {
        alignSelf: 'flex-end',
        justifyContent: 'flex-end'
        // top: 33,
        // position: 'absolute'
    },

    id: {
        color: '#9E9E9E',
        fontWeight: 'bold',
    },

    name: {
        fontWeight: 'bold',
        marginLeft: 10,
    },

    icone: {
        height: 35,
        width: 17,
        resizeMode: 'contain',
    },

    stat: {
        margin: 10,
    },
})

export default PokemonCard