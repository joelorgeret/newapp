import React from "react"
import { LogBox } from "react-native"
import { StyleSheet, View } from "react-native"
import { useSafeAreaInsets } from "react-native-safe-area-context"
import Header from "../components/Header"
import PokemonList from "../components/PokemonList"

LogBox.ignoreLogs(['Setting a timer']);

const MainView: React.FC = ({ children }) => {
    const safeArea = useSafeAreaInsets()

    return <View style={{
        paddingTop: safeArea.top,
        flex: 1,
    }}>
        <View style={styles.container}>
            <View style={styles.header}>
                <Header />
            </View>
            <View style={styles.liste}>
                <PokemonList></PokemonList>
            </View>
        </View>{children}
    </View>
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#E5E5E5',
        // paddingTop: 40,
        // alignItems: 'stretch',
        // justifyContent: 'flex-start',
    },

    header: {
        flex: 1,
        minHeight: 80,
        // alignSelf: "stretch",
    },

    liste: {
        flex: 4,
        elevation: 3,
    }
});

export default MainView