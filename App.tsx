import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { SafeAreaProvider, useSafeAreaInsets } from 'react-native-safe-area-context';
import React from 'react'
import AppContextProvider from './context/AppContext'
import MainView from './pages/MainView';



export default function App() {


  return (
    <SafeAreaProvider >
      <AppContextProvider>
        <MainView />
      </AppContextProvider>
    </SafeAreaProvider>
  );
}

